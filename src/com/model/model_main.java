/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.controller.controller_main;
import com.view.FrmMainMenu;
import java.sql.SQLException;
import Conn.koneksi;
import com.view.FrmMenuPencarian;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.Year;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class model_main implements controller_main {
    Connection conn;
    Statement stt;
    PreparedStatement pst;
    ResultSet res;
    public DefaultTableModel model;
    
    public void getLableState(FrmMainMenu frm) {
        frm.jLabel7.setVisible(false);
    }
    
    public void getSetBtnState(FrmMainMenu frm) {
        frm.btn_set.setVisible(false);
    }
    
    public void getModel(FrmMainMenu frm) {
        model = new DefaultTableModel();
        frm.tbl_tampilBuku.setModel(model);
        
        model.addColumn("ID");
        model.addColumn("JUDUL");
        model.addColumn("TAHUN TERBIT");
        model.addColumn("KATEGORI");
        model.addColumn("PENGARANG");
        model.addColumn("PENERBIT");
        frm.tbl_tampilBuku.setDefaultEditor(Object.class, null);
    }
    
    public void getDataTable(FrmMainMenu frm) {
        getModel(frm);
        model.getDataVector();
        model.fireTableDataChanged();
        String query = "SELECT kodeBuku,judul,YEAR(tahunTerbit),kategori.namaKategori,pengarang.namaPengarang,penerbit.namaPenerbit FROM buku \n" +
                        "	JOIN kategori ON (buku.kodeKategori = kategori.kodeKategori)\n" +
                        "	JOIN pengarang ON (buku.kodePengarang = pengarang.kodePengarang)\n" +
                        "	JOIN penerbit ON (buku.kodePenerbit = penerbit.kodePenerbit)";
        try {
            conn = (Connection)koneksi.getKoneksi();
            stt = conn.createStatement();
            res = stt.executeQuery(query);
            while(res.next()) {
                Object[] obj = new Object[6];
                    obj[0] = res.getString("kodeBuku");
                    obj[1] = res.getString("judul");
                    obj[2] = res.getString("YEAR(tahunTerbit)");
                    obj[3] = res.getString("kategori.namaKategori");
                    obj[4] = res.getString("pengarang.namaPengarang");
                    obj[5] = res.getString("penerbit.namaPenerbit");
                    model.addRow(obj);
            }
            stt.close();
            res.close();
        } catch (SQLException ex) {
            Logger.getLogger(model_main.class.getName()).log(Level.SEVERE, "ERR MODEL + ", ex.getMessage());
        }
    }
    
    public void getCbbItem(FrmMainMenu frm) {
        frm.cbb_kategori.removeAllItems();
        frm.cbb_penerbit.removeAllItems();
        frm.cbb_pengarang.removeAllItems();
    }
    
    public void getNamaKategori(FrmMainMenu frm) { // cbb Kategori
        conn = (Connection)koneksi.getKoneksi();
        try {
            stt = conn.createStatement();
            res = stt.executeQuery("SELECT namaKategori FROM kategori;");
            while(res.next()) {
                frm.cbb_kategori.addItem(res.getString("namaKategori"));
            }
            stt.close();
            res.close();
        } catch (SQLException ex) {
            Logger.getLogger(model_main.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
    }
    
    public void getKodeKategori(FrmMainMenu frm) { //txt kategori
        conn = (Connection)koneksi.getKoneksi();
        try {
            stt = conn.createStatement();
            res = stt.executeQuery("SELECT kodeKategori FROM kategori WHERE namaKategori = '"+frm.cbb_kategori.getSelectedItem()+"';");
            while(res.next()) {
                frm.txt_Kategori.setText(res.getString("kodeKategori"));
            }
            stt.close();
            res.close();
        } catch (SQLException ex) {
            Logger.getLogger(model_main.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
    }
    
    public void getNamaPengarang(FrmMainMenu frm) { //cbb pengarang
        conn = (Connection)koneksi.getKoneksi();
        try {
            stt = conn.createStatement();
            res = stt.executeQuery("SELECT namaPengarang FROM pengarang");
            while(res.next()) {
                frm.cbb_pengarang.addItem(res.getString("namaPengarang"));
            }
            stt.close();
            res.close();
        } catch (SQLException ex) {
            Logger.getLogger(model_main.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
    }
    
    public void getKodePengarang(FrmMainMenu frm) { //txt pengarang
        conn = (Connection)koneksi.getKoneksi();
        try {
            stt = conn.createStatement();
            res = stt.executeQuery("SELECT kodePengarang FROM pengarang WHERE namaPengarang LIKE '%"+frm.cbb_pengarang.getSelectedItem()+"%';");
            while(res.next()) {
                frm.txt_Pengarang.setText(res.getString("kodePengarang"));
            }
            stt.close();
            res.close();
        } catch (SQLException ex) {
            Logger.getLogger(model_main.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
    }
    
    public void getNamaPenerbit(FrmMainMenu frm) { //cbb penerbit
        conn = (Connection)koneksi.getKoneksi();
        try {
            stt = conn.createStatement();
            res = stt.executeQuery("SELECT namaPenerbit,kodePenerbit FROM penerbit");
            while(res.next()) {
                frm.cbb_penerbit.addItem(res.getString("namaPenerbit"));
//                frm.txt_Penerbit.setText(res.getString("kodePenerbit"));
            }
            stt.close();
            res.close();
        } catch (SQLException ex) {
            Logger.getLogger(model_main.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
    }
    
    public void getKodePenerbit(FrmMainMenu frm) { //txt penerbit
        conn = (Connection)koneksi.getKoneksi();
        try {
            stt = conn.createStatement();
            res = stt.executeQuery("SELECT kodePenerbit FROM penerbit WHERE namaPenerbit LIKE '%"+frm.cbb_penerbit.getSelectedItem()+"%';");
            while(res.next()) {
                frm.txt_Penerbit.setText(res.getString("kodePenerbit"));
            }
            stt.close();
            res.close();
        } catch (SQLException ex) {
            Logger.getLogger(model_main.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
    }
    
    public void componenReset(FrmMainMenu frm) {
        frm.txt_idBuku.setText("");
        frm.txt_judul.setText("");
        frm.txt_tahunTerbit.setText("");
    }
    
    @Override
    public void tambah(FrmMainMenu frm) throws SQLException {
        boolean emptyID = false;
        if(frm.txt_idBuku.getText().isEmpty() == emptyID) {
            JOptionPane.showMessageDialog(frm, ("buku dengan ID "+frm.txt_idBuku.getText()+" Sudah ada xD"));
        } else if (frm.txt_judul.getText().isEmpty() || frm.txt_tahunTerbit.getText().isEmpty()) {
            JOptionPane.showMessageDialog(frm, ("Silahkan Periksa Inputan Anda xD"));
        } else {
            String query = "INSERT INTO buku(judul,tahunTerbit,kodeKategori,kodePengarang,kodePenerbit) "
                        + "VALUES('"+frm.txt_judul.getText().replaceAll("\\s+", " ").trim()+"','"+frm.txt_tahunTerbit.getText().trim()+"',"
                        + "'"+frm.txt_Kategori.getText()+"',"+frm.txt_Pengarang.getText()+","+frm.txt_Penerbit.getText()+");";
//            String Year = null;
            if(frm.txt_tahunTerbit.getText().length() < 0) {
                JOptionPane.showMessageDialog(null, "Masukan Tahun Dengan Format YYYY,\neg:1945");
            } else {
                getDataTable(frm);
                frm.jLabel7.setVisible(true);
                frm.jLabel7.setText("State Tambah");
                conn = (Connection)koneksi.getKoneksi();
                pst = conn.prepareStatement(query);
                pst.execute();
                JOptionPane.showMessageDialog(frm, "Data dengan judul \n • "+frm.txt_judul.getText().replaceAll("\\s+", " ").trim());
                pst.close();
                componenReset(frm);
            }
        }
    }

    @Override
    public void edit(FrmMainMenu frm) throws SQLException {
        boolean state = true;
        if (state = frm.txt_idBuku.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Silahkan pilih Data");
        } else {
            frm.jLabel7.setVisible(true);
            frm.jLabel7.setText("state Editing !");
            getCbbItem(frm);
            frm.btn_hapus.setEnabled(false);
            frm.btn_tambah.setEnabled(false);
            frm.btn_set.setVisible(true);
            getNamaKategori(frm);
            getNamaPenerbit(frm);
            getNamaPengarang(frm);
            getKodeKategori(frm);
            getKodePenerbit(frm);
            getKodePengarang(frm);
        }
    }
    
    public void setEdit(FrmMainMenu frm) throws SQLException {
        boolean state = true;
        if (state = frm.txt_idBuku.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Silahkan pilih Data");
        } else {
            String query = "UPDATE buku SET judul='"+frm.txt_judul.getText().replaceAll("\\s+", " ").trim()+"'"
                                + ",tahunTerbit='"+frm.txt_tahunTerbit.getText().replaceAll("\\s+", " ").trim()+"'"
                                + ",kodeKategori='"+frm.txt_Kategori.getText()+"'"
                                + ",kodePengarang="+frm.txt_Pengarang.getText()+""
                                + ",kodePenerbit="+frm.txt_Penerbit.getText()+""
                            + " WHERE kodeBuku="+frm.txt_idBuku.getText()+";";
            conn = (Connection)koneksi.getKoneksi();
            pst = conn.prepareStatement(query);
            pst.execute();
            frm.jLabel7.setVisible(true);
            frm.jLabel7.setText("state Edited");
            JOptionPane.showMessageDialog(null, "Data telah di update");
            pst.close();
            getDataTable(frm);
        }
        frm.btn_hapus.setEnabled(true);
        frm.btn_tambah.setEnabled(true);
        frm.btn_set.setEnabled(false);
    }

    @Override
    public void hapus(FrmMainMenu frm) throws SQLException {
        boolean state = false;
        if(frm.txt_idBuku.getText().isEmpty() == state ) {
            frm.jLabel7.setVisible(true);
            frm.jLabel7.setText("State Hapus");
            String query = "DELETE FROM buku WHERE kodeBuku = "+frm.txt_idBuku.getText()+";";
            conn = (Connection)koneksi.getKoneksi();
            pst = conn.prepareStatement(query);
            pst.execute();
            componenReset(frm);
            JOptionPane.showMessageDialog(null, "Data "+frm.txt_idBuku.getText()+" Berhasil Dihapus");
            getDataTable(frm);
        } else {
            JOptionPane.showMessageDialog(null, "Silahkan pilih data !");
        }
    }

    @Override
    public void refresh(FrmMainMenu frm) throws SQLException {
        getCbbItem(frm);
        getKodeKategori(frm);
        getKodePenerbit(frm);
        getKodePengarang(frm);
        getNamaKategori(frm);
        getNamaPenerbit(frm);
        getNamaPengarang(frm);
        getDataTable(frm);
        componenReset(frm);
        frm.btn_hapus.setEnabled(true);
        frm.btn_tambah.setEnabled(true);
        frm.btn_set.setVisible(false);
//        getLableState(fmm);
        frm.jLabel7.setVisible(true);
        frm.jLabel7.setText("State Refresh");
    }

    @Override
    public void cari(FrmMainMenu frm) {
        frm.dispose();
        FrmMenuPencarian getState = new FrmMenuPencarian();
        getState.setVisible(true);
    }

    @Override
    public void exit(FrmMainMenu frm) {
        conn = (Connection)koneksi.getKoneksi();
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(model_main.class.getName()).log(Level.SEVERE, null, ex);
        }
        frm.dispose();
    }

    @Override
    public void tbl_click(FrmMainMenu frm) throws SQLException {
        int row = frm.tbl_tampilBuku.getSelectedRow();
        getCbbItem(frm);
        String tblClick = (frm.tbl_tampilBuku.getModel().getValueAt(row, 0).toString());
        String query = "SELECT buku.kodeBuku,buku.judul,YEAR(buku.tahunTerbit),\n" +
                        "	kategori.kodeKategori,kategori.namaKategori,\n" +
                        "	pengarang.kodePengarang,pengarang.namaPengarang,\n" +
                        "	penerbit.kodePenerbit,penerbit.namaPenerbit \n" +
                        "FROM buku JOIN kategori ON (buku.kodeKategori = kategori.kodeKategori)\n" +
                        "	JOIN penerbit ON (buku.kodePenerbit = penerbit.kodePenerbit)\n" +
                        "	JOIN pengarang ON (buku.kodePengarang = pengarang.kodePengarang)\n" +
                        "WHERE buku.kodeBuku = '"+tblClick+"';";
        conn = (Connection)koneksi.getKoneksi();
        stt = conn.createStatement();
        res = stt.executeQuery(query);
        while(res.next()) {
            frm.txt_idBuku.setText(res.getString("buku.kodeBuku"));
            frm.txt_judul.setText(res.getString("buku.judul"));
            frm.txt_tahunTerbit.setText(res.getString("YEAR(buku.tahunTerbit)"));
            frm.cbb_kategori.addItem(res.getString("kategori.namaKategori"));
            frm.cbb_penerbit.addItem(res.getString("penerbit.namaPenerbit"));
            frm.cbb_pengarang.addItem(res.getString("pengarang.namaPengarang"));
//            frm.txt_Kategori.setText(res.getString("kategori.kodeKategori"));
//            frm.txt_Penerbit.setText(res.getString("penerbit.kodePenerbit"));
//            frm.txt_Pengarang.setText(res.getString("pengarang.kodePengarang"));
        }
        res.close();
        stt.close();
//            getNamaKategori(frm);
//            getNamaPenerbit(frm);
//            getNamaPengarang(frm);
        frm.jLabel7.setVisible(true);
        frm.jLabel7.setText("Data Selected");
    }    
}
