/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.controller.*;
import com.view.FrmManager;
import java.sql.SQLException;

import Conn.koneksi;
import com.view.FrmMainMenu;
import com.view.FrmEditor;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Dz
 */
public class model_manager implements controller_manager {
    private Connection conn;
    private Statement stt;
    private ResultSet res;
    
    private DefaultTableModel model;
    
    public String[] cbItem = {"Pilih Menu Editor","Kategori","Penerbit","Pengarang"};
        
    public void getLableState(FrmManager frm) {
        frm.lbl_status.setVisible(false);
    }
    
    public void getModelTable(FrmManager frm) {
        model = new DefaultTableModel();
        frm.tbl_manage.setModel(model);
        if(frm.cbb_manage.getSelectedItem() == cbItem[0]){
            frm.lbl_status.setText("NullPo");
        }else if(frm.cbb_manage.getSelectedItem() == cbItem[1]){
            frm.lbl_status.setText(cbItem[1]);
            frm.lbl_status.setVisible(true);
            model.addColumn("ID Kategori");
            model.addColumn("Kategori Buku");
        }else if(frm.cbb_manage.getSelectedItem() == cbItem[2]){
            frm.lbl_status.setText(cbItem[2]);
            frm.lbl_status.setVisible(true);
            model.addColumn("ID Penerbit");
            model.addColumn("Nama Penerbit");
        }else if(frm.cbb_manage.getSelectedItem() == cbItem[3]){
            frm.lbl_status.setText(cbItem[3]);
            frm.lbl_status.setVisible(true);
            model.addColumn("ID Pengarang");
            model.addColumn("Nama Pengarang");
        }
        frm.tbl_manage.setDefaultEditor(Object.class, null);
    }
    
    public void getCbbItem(FrmManager frm) {
        frm.cbb_manage.removeAllItems();
        frm.cbb_manage.addItem(cbItem[0]);
        frm.cbb_manage.addItem(cbItem[1]);
        frm.cbb_manage.addItem(cbItem[2]);
        frm.cbb_manage.addItem(cbItem[3]);
        frm.cbb_manage.setSelectedItem(cbItem[1]);
    }
    
    public void loadKategori(FrmManager frm) throws SQLException { //item kategori
        getModelTable(frm);
            model.getDataVector().removeAllElements();
            model.fireTableDataChanged();
            String query = "Select kodeKategori,namaKategori from kategori";
            conn = (Connection)koneksi.getKoneksi();
            stt = conn.createStatement();
            res = stt.executeQuery(query);
            while(res.next()) {
                Object obj[] = new Object[2];
                    obj[0] = res.getString("kodeKategori");
                    obj[1] = res.getString("namaKategori");
                model.addRow(obj);
            } stt.close(); res.close();
    }
    
    public void loadPenerbit(FrmManager frm) throws SQLException { //item penerbit
        getModelTable(frm);
                model.getDataVector().removeAllElements();
                model.fireTableDataChanged();
                String query = "Select kodePenerbit,namaPenerbit from penerbit";
                conn = (Connection)koneksi.getKoneksi();
                stt = conn.createStatement();
                res = stt.executeQuery(query);
                while(res.next()) {
                    Object obj[] = new Object[2];
                        obj[0] = res.getString("kodePenerbit");
                        obj[1] = res.getString("namaPenerbit");
                    model.addRow(obj);
                } stt.close(); res.close();
    }
    
    public void loadPengarang(FrmManager frm) throws SQLException { //item pengarang
        getModelTable(frm);
            model.getDataVector().removeAllElements();
            model.fireTableDataChanged();
            String query = "Select kodePengarang,namaPengarang from pengarang";
            conn = (Connection)koneksi.getKoneksi();
            stt = conn.createStatement();
            res = stt.executeQuery(query);
            while(res.next()) {
                Object obj[] = new Object[2];
                    obj[0] = res.getString("kodePengarang");
                    obj[1] = res.getString("namaPengarang");
                model.addRow(obj);
            } stt.close(); res.close();
    }
    
    @Override
    public void getDataTable(FrmManager frm) throws SQLException {
        if(frm.cbb_manage.getSelectedItem() == cbItem[1]) {
            loadKategori(frm);
        } else if(frm.cbb_manage.getSelectedItem() == cbItem[2]) {
            loadPenerbit(frm);
        } else if(frm.cbb_manage.getSelectedItem() == cbItem[3]) {
            loadPengarang(frm);
        }
    }

    @Override
    public void btnTambah(FrmManager frm) { //lempar ke View tambah
        FrmEditor util = new FrmEditor();
        util.setVisible(true);
        util.btn_manage.setText("Tambah");
    }

    @Override
    public void btnEdit(FrmManager frm) { //lempar data table ke form editor
        FrmEditor util = new FrmEditor();
        util.cbb_Atribut.disable();
        util.btn_manage.setText("Edit");
        
        int row = frm.tbl_manage.getSelectedRow(); //inisial baris dari table
        
        if(frm.cbb_manage.getSelectedItem() == cbItem[0]) {
            JOptionPane.showMessageDialog(frm, "Pilih daftar menu");
        } else if (row < 0) {
            JOptionPane.showMessageDialog(null, "silahkan pilih data edit xD");
        } else if (row >= 0) {
            if(frm.cbb_manage.getSelectedItem() == cbItem[1]) {
                util.cbb_Atribut.setSelectedItem(cbItem[1]);
                util.setVisible(true);
                util.lbl_status.setText("Edit Data "+cbItem[1]);
                util.txt_idKategori.disable();

                String tblClick = (frm.tbl_manage.getModel().getValueAt(row, 0).toString());
                conn = (Connection)koneksi.getKoneksi();
                try {
                    stt = conn.createStatement();
                    res = stt.executeQuery("SELECT kodeKategori,namaKategori FROM kategori WHERE kodeKategori = '"+tblClick+"';");
                    while(res.next()) {
                        util.txt_idKategori.setText(res.getString("kodeKategori"));
                        util.txt_manager.setText(res.getString("namaKategori"));
                    }
                    stt.close();
                    res.close();
                } catch (SQLException ex) {
                    Logger.getLogger(model_manager.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (frm.cbb_manage.getSelectedItem() == cbItem[2]) {
                util.cbb_Atribut.setSelectedItem(cbItem[2]);
                util.setVisible(true);
                util.lbl_status.setText("Edit Data "+cbItem[2]);
                util.txt_idKategori.setVisible(true);
                util.txt_idKategori.disable();
                
                String tblClick = (frm.tbl_manage.getModel().getValueAt(row, 0).toString());
                conn = (Connection)koneksi.getKoneksi();
                try {
                    stt = conn.createStatement();
                    res = stt.executeQuery("SELECT kodePenerbit,namaPenerbit FROM penerbit WHERE kodePenerbit = "+tblClick+";");
                    while(res.next()) {
                        util.txt_idKategori.setText(res.getString("kodePenerbit"));
                        util.txt_manager.setText(res.getString("namaPenerbit"));
                    }
                    stt.close();
                    res.close();
                } catch (SQLException ex) {
                    Logger.getLogger(model_manager.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (frm.cbb_manage.getSelectedItem() == cbItem[3]) {
                util.cbb_Atribut.setSelectedItem(cbItem[3]);
                util.setVisible(true);
                util.lbl_status.setText("Edit Data "+cbItem[3]);
                util.txt_idKategori.setVisible(true); 
                util.txt_idKategori.disable();

                String tblClick = (frm.tbl_manage.getModel().getValueAt(row, 0).toString());
                conn = (Connection)koneksi.getKoneksi();
                try {
                    stt = conn.createStatement();
                    res = stt.executeQuery("SELECT kodePengarang,namaPengarang FROM pengarang WHERE kodePengarang = "+tblClick+";");
                    while(res.next()) {
                        util.txt_idKategori.setText(res.getString("kodePengarang"));
                        util.txt_manager.setText(res.getString("namaPengarang"));
                    }
                    stt.close();
                    res.close();
                } catch (SQLException ex) {
                    Logger.getLogger(model_manager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public void reload(FrmManager frm) throws SQLException {
        getDataTable(frm);
        JOptionPane.showMessageDialog(null, "Data telah di refresh");
    }

    @Override
    public void selesai(FrmManager frm) {
        FrmMainMenu menu = new FrmMainMenu();
        menu.setVisible(true);
        frm.dispose();
    }
    
}
