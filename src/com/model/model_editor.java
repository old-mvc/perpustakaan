/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import Conn.koneksi;
import com.view.FrmEditor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import com.controller.controller_editor;
import com.view.FrmManager;

/**
 *
 * @author Dz
 */
public class model_editor implements controller_editor {
    
    private final Connection conn = (Connection)koneksi.getKoneksi();
    public String cbItem[] = {"Kategori","Penerbit","Pengarang"};
        
    @Override
    public void EditTambah(FrmEditor frm) {
//        model_manager get = new model_manager();
//        FrmManager frmManager = new FrmManager();
        
        if(frm.btn_manage.getText().equals("Tambah")){ //indikasi button tambah
            if(cbItem[0] == frm.cbb_Atribut.getSelectedItem()) { //cek id buku 3 karakter
                if(frm.txt_idKategori.getText().length() >= 4) {
                    JOptionPane.showMessageDialog(frm, "WOI KELEBIHAN !!");
                } else if(frm.txt_idKategori.getText().length() <= 3) {
                    if(frm.txt_idKategori.getText().isEmpty() || frm.txt_manager.getText().isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Cek Datanya gih xD");
                    } else {
                        try {
                            String query = "INSERT INTO kategori VALUES('"+frm.txt_idKategori.getText()
                                                                            .replaceAll("\\s+", " ")
                                                                            .trim()+"','"
                                                +frm.txt_manager.getText()
                                                        .replaceAll("\\s+", " ")
                                                        .trim()+"')";
                            PreparedStatement pst = conn.prepareStatement(query);
                            pst.execute();
                            JOptionPane.showMessageDialog(null, ("Data \n"+"• "+frm.txt_idKategori.getText()+"\n• "
                                                +frm.txt_manager.getText().replaceAll("\\s+", " ").trim()+"\nTersimpan ke Kategori"));
//                            if(JOptionPane.OK_OPTION)
                            pst.close();
                        } catch (SQLException ex) {
                            Logger.getLogger(model_editor.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            } //batas kategori
            else if (cbItem[1] == frm.cbb_Atribut.getSelectedItem()) {
                    try {
                        String query = "INSERT INTO penerbit(namaPenerbit) VALUES('"+frm.txt_manager.getText()
                                                                                    .replaceAll("\\s+", " ")
                                                                                    .trim()+"')";
                        PreparedStatement pst = conn.prepareStatement(query);
                        pst.execute();
                        JOptionPane.showMessageDialog(null, ("Data \n• "+frm.txt_manager.getText().replaceAll("\\s+", " ").trim()+"\nTersimpan ke Penerbit"));
                        pst.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(model_editor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (cbItem[2] == frm.cbb_Atribut.getSelectedItem()) {
                    try {
                        String query = "INSERT INTO pengarang(namaPengarang) VALUES('"+frm.txt_manager.getText()
                                                                                        .replaceAll("\\s+", " ")
                                                                                        .trim()+"')";
                        PreparedStatement pst = conn.prepareStatement(query);
                        pst.execute();
                        JOptionPane.showMessageDialog(null, ("Data \n• "+frm.txt_manager.getText().replaceAll("\\s+", " ").trim()+"\nTersimpan ke Pengarang"));
                        pst.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(model_editor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else if (frm.btn_manage.getText().equals("Edit")) { //indikasi button edit
                if(frm.cbb_Atribut.getSelectedItem() == cbItem[0]) {
//                    JOptionPane.showMessageDialog(null, "kategori");
                    String query = "UPDATE kategori SET namaKategori ='"+frm.txt_manager.getText().replaceAll("\\s+", " ").trim()+"' WHERE kodeKategori = '"+frm.txt_idKategori.getText()+"';";
                    try {
                        PreparedStatement pst = conn.prepareStatement(query);
                        pst.execute();
                        JOptionPane.showMessageDialog(null, ("Data "+cbItem[0]+"\n"+"• "+frm.txt_idKategori.getText()+"\n• "+frm.txt_manager.getText()+"\nTelah di update"));
                        pst.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(model_editor.class.getName()).log(Level.SEVERE, null, ex.getMessage());
                    }
                } else if(frm.cbb_Atribut.getSelectedItem() == cbItem[1]) {
//                    JOptionPane.showMessageDialog(null, "penerbit");
                    String query = "UPDATE penerbit SET namaPenerbit ='"+frm.txt_manager.getText().replaceAll("\\s+", " ").trim()+"' WHERE kodePenerbit = "+frm.txt_idKategori.getText()+";";
                    try {
                        PreparedStatement pst = conn.prepareStatement(query);
                        pst.execute();
                        JOptionPane.showMessageDialog(null, ("Data "+cbItem[1]+"\n• "+frm.txt_manager.getText()+"\nTelah di update"));
                        pst.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(model_editor.class.getName()).log(Level.SEVERE, null, ex.getMessage());
                    }
                } else if(frm.cbb_Atribut.getSelectedItem() == cbItem[2]) {
//                    JOptionPane.showMessageDialog(null, "pengarang");
                    String query = "UPDATE pengarang SET namaPengarang ='"+frm.txt_manager.getText().replaceAll("\\s+", " ")+"' WHERE kodePengarang = "+frm.txt_idKategori.getText()+";";
                    try {
                        PreparedStatement pst = conn.prepareStatement(query);
                        pst.execute();
                        JOptionPane.showMessageDialog(null, ("Data "+cbItem[2]+"\n• "+frm.txt_manager.getText()+"\nTelah di update"));
                        pst.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(model_editor.class.getName()).log(Level.SEVERE, null, ex.getMessage());
                    }
                }
            }
            /*try {
                get.getDataTable(frmManager);
            } catch (SQLException ex) {
                Logger.getLogger(model_editor.class.getName()).log(Level.SEVERE, null, ex);
            }*/
//            frm.dispose();
    }

    @Override
    public void cancle(FrmEditor frm) {
        frm.dispose();
    }
    
    public void cbItem(FrmEditor frm) {
        frm.cbb_Atribut.removeAllItems();
        frm.cbb_Atribut.addItem(cbItem[0]);
        frm.cbb_Atribut.addItem(cbItem[1]);
        frm.cbb_Atribut.addItem(cbItem[2]);
    }
    
    public void getCbState(FrmEditor frm) {
        if(cbItem[0] == frm.cbb_Atribut.getSelectedItem()) {
            frm.lbl_id.setVisible(true); frm.lbl_id.setText("ID Kategori");
            frm.txt_idKategori.setVisible(true); frm.lbl_nama.setText("Nama Kategori");
            frm.lbl_status.setText("tambah data "+cbItem[0]);
        } else if (cbItem[1] == frm.cbb_Atribut.getSelectedItem()) {
            frm.lbl_id.setVisible(false);
            frm.txt_idKategori.setVisible(false); frm.lbl_nama.setText("Nama Penerbit");
            frm.lbl_status.setText("tambah data "+cbItem[1]);
        } else if (cbItem[2] == frm.cbb_Atribut.getSelectedItem()) {
            frm.lbl_id.setVisible(false);
            frm.txt_idKategori.setVisible(false); frm.lbl_nama.setText("Nama Pengarang");
            frm.lbl_status.setText("tambah data "+cbItem[2]);
        }
    }
}
