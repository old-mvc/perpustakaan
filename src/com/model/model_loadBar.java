
package com.model;

import com.controller.controller_loadBar;
import com.view.FrmProgress;
import java.util.logging.Level;
import java.util.logging.Logger;

public class model_loadBar implements controller_loadBar {
    private int width = 160;
    private int heigh = 27;
    
    @Override
    public void getLoader(FrmProgress frm) throws InterruptedException {
        frm.jb.setBounds(5,5,150,18);
        frm.jb.setValue(0);
        frm.jb.setStringPainted(true);
        frm.add(frm.jb);
        frm.setSize(width,heigh);
//        frm.setLayout(null);
    }
    
    public void getUpdateValue(FrmProgress frm) {
        int i=0,max=100;
        while(i<=max) {
            frm.jb.setValue(i);
            i=i+1;
            try {
                Thread.sleep(15);
            } catch (InterruptedException ex) {
                Logger.getLogger(model_loadBar.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
