/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import Conn.koneksi;
import com.controller.controller_cari;
import com.view.FrmMainMenu;
import com.view.FrmMenuPencarian;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Dz
 */
public class model_cari implements controller_cari {
    private Connection conn;
    private Statement stt;
    private ResultSet res;
    
    private DefaultTableModel model;
    
    public void tblModel(FrmMenuPencarian frm) {
        model = new DefaultTableModel();
        frm.tbl_cariBuku.setModel(model);
        
        model.addColumn("ID");
        model.addColumn("JUDUL");
        model.addColumn("TAHUN TERBIT");
        model.addColumn("KATEGORI");
        model.addColumn("PENGARANG");
        model.addColumn("PENERBIT");
    }

    public void getDataTable(FrmMenuPencarian frm) throws SQLException {
        tblModel(frm);
        model.getDataVector();
        model.fireTableDataChanged();
        String query = "SELECT kodeBuku,judul,tahunTerbit,kategori.namaKategori,pengarang.namaPengarang,penerbit.namaPenerbit FROM buku \n" +
                        "	JOIN kategori ON (buku.kodeKategori = kategori.kodeKategori)\n" +
                        "	JOIN pengarang ON (buku.kodePengarang = pengarang.kodePengarang)\n" +
                        "	JOIN penerbit ON (buku.kodePenerbit = penerbit.kodePenerbit)";
        conn = (Connection)koneksi.getKoneksi();
        stt = conn.createStatement();
        res = stt.executeQuery(query);
        while(res.next()) {
            Object obj[] = new Object[6];
                obj[0] = res.getString("kodeBuku");
                obj[1] = res.getString("judul");
                obj[2] = res.getString("tahunTerbit");
                obj[3] = res.getString("kategori.namaKategori");
                obj[4] = res.getString("pengarang.namaPengarang");
                obj[5] = res.getString("penerbit.namaPenerbit");
                model.addRow(obj);
        } stt.close(); res.close();
    }
    
    @Override
    public void refresh(FrmMenuPencarian frm) throws SQLException {
        getDataTable(frm);
        frm.txt_cari.setText("");
    }

    @Override
    public void cari(FrmMenuPencarian frm) throws SQLException {
        String cari = frm.txt_cari.getText();
        tblModel(frm);
        model.getDataVector();
        model.fireTableDataChanged();
        String query = "SELECT buku.kodeBuku,buku.judul,buku.tahunTerbit,kategori.namaKategori,penerbit.namaPenerbit,pengarang.namaPengarang FROM buku \n" +
                                    "	JOIN kategori ON (buku.kodeKategori = kategori.kodeKategori) \n" +
                                    "	JOIN pengarang ON (buku.kodePengarang = pengarang.kodePengarang) \n" +
                                    "	JOIN penerbit ON (buku.kodePenerbit = penerbit.kodePenerbit)\n" +
                                    "WHERE buku.kodeBuku = '"+cari+"' \n" +
                                    "OR buku.judul LIKE '%"+cari+"%' \n" +
                                    "OR buku.tahunTerbit = '"+cari+"' \n" +
                                    "OR kategori.namaKategori LIKE '%"+cari+"%' \n" +
                                    "OR penerbit.namaPenerbit LIKE '%"+cari+"%' \n" +
                                    "OR pengarang.namaPengarang LIKE '%"+cari+"%';";
        conn = (Connection)koneksi.getKoneksi();
        stt = conn.createStatement();
        res = stt.executeQuery(query);
        while(res.next()) {
            Object obj[] = new Object[6];
                obj[0] = res.getString("buku.kodeBuku");
                obj[1] = res.getString("buku.judul");
                obj[2] = res.getString("buku.tahunTerbit");
                obj[3] = res.getString("kategori.namaKategori");
                obj[4] = res.getString("pengarang.namapengarang");
                obj[5] = res.getString("penerbit.namapenerbit");
                model.addRow(obj);
        } stt.close(); res.close();
    }

    @Override
    public void back(FrmMenuPencarian frm) {
        frm.dispose();
        FrmMainMenu mainFrm = new FrmMainMenu();
        mainFrm.setVisible(true);
    }

    @Override
    public void exit(FrmMenuPencarian frm) {
        frm.dispose();
    }
    
}
