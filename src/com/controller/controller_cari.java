/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.view.FrmMenuPencarian;
import java.sql.SQLException;

/**
 *
 * @author Dz
 */
public interface controller_cari {
    public void refresh(FrmMenuPencarian frm) throws SQLException;
    public void cari(FrmMenuPencarian frm) throws SQLException;
    public void back(FrmMenuPencarian frm);
    public void exit(FrmMenuPencarian frm);
}
