/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.view.FrmManager;
import java.sql.SQLException;

/**
 *
 * @author Dz
 */
public interface controller_manager {
    
    public void getDataTable(FrmManager frm) throws SQLException;
    public void btnTambah(FrmManager frm); 
    public void btnEdit(FrmManager frm); 
    public void reload(FrmManager frm) throws SQLException;
    public void selesai(FrmManager frm);
}
