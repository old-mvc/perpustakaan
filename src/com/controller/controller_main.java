/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.view.FrmMainMenu;
import java.sql.SQLException;

/**
 *
 * @author Dz
 */
public interface controller_main {
    public void tambah(FrmMainMenu frm) throws SQLException;
    public void edit(FrmMainMenu frm) throws SQLException;
    public void hapus(FrmMainMenu frm) throws SQLException;
    public void refresh(FrmMainMenu fmm) throws SQLException;
    public void cari(FrmMainMenu frm);
    public void exit(FrmMainMenu frm);
    public void tbl_click(FrmMainMenu frm) throws SQLException;
}
