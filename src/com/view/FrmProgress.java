
package com.view;

import com.model.model_loadBar;
import static java.lang.Thread.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JProgressBar;

public class FrmProgress extends javax.swing.JFrame {    
    public JProgressBar jb = new JProgressBar(0, 100);
        
    public FrmProgress() {
        initComponents();
        loader();
        updateBar();
        this.dispose();
    }
    
    public void loader() {
        model_loadBar state = new model_loadBar();
        try {
            state.getLoader(this);
        } catch (InterruptedException ex) {
            Logger.getLogger(FrmProgress.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateBar() {
        model_loadBar update = new model_loadBar();
        update.getUpdateValue(this);
    }
    
//    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        setUndecorated(true);
        setResizable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 146, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 85, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) throws InterruptedException {
        FrmProgress frm = new FrmProgress();
//        frm.initComponents();
        frm.setVisible(true);
//        frm.loader();
        frm.updateBar();
        frm.dispose();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
