/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dz
 */
public class koneksi {
    public static Connection conn;
    public static Connection getKoneksi() {
        String URL = "jdbc:mysql://127.0.0.1/perpustakaan",
               USR = "root",
               PWD = "";
        try {
            conn = (Connection)DriverManager.getConnection(URL,USR,PWD);
        } catch (SQLException ex) {
            try {
                Logger.getLogger(koneksi.class.getName()).log(Level.SEVERE, "ERR KONEKSI !! + ", ex.getMessage());
                conn.close();
            } catch (SQLException ex1) {
                Logger.getLogger(koneksi.class.getName()).log(Level.SEVERE, "Closing Connection . . .\n", ex1.getMessage());
            }
//            return conn;
        }
        return conn;
    }
}
